<?php
session_start();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  include 'config.php';

  // Pastikan Anda telah membuat koneksi ke database
  $conn = new mysqli($host, $username, $password, $dbname);

  if ($conn->connect_error) {
    die("Koneksi ke database gagal: " . $conn->connect_error);
  }
 
  if (
    isset($_POST['name']) && isset($_POST['eventDate']) &&
    isset($_POST['roomType']) && !empty($_POST['name']) &&
    !empty($_POST['eventDate']) && !empty($_POST['roomType'])
  ) {
    $name = $_POST['name'];
    $eventDate = $_POST['eventDate'];
    $roomType = $_POST['roomType'];

    $sql = "INSERT INTO orders (name, event_date, room_type) VALUES ('$name', '$eventDate', '$roomType')";

    if ($conn->query($sql) === true) {
      $_SESSION['success_message'] = 'Pesanan berhasil disimpan.';
    } else {
      $_SESSION['error_message'] = 'Gagal menyimpan pesanan: ' . $conn->error;
    }

    $conn->close();
  } else {
    $_SESSION['error_message'] = 'Mohon lengkapi semua informasi pesanan.';
  }
}

header('Location: orders.php');
