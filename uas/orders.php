<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Daftar Pesanan - Penyewaan Gedung XYZ</title>
  <link rel="stylesheet" href="style.css">
</head> 
<body>
  <header>
    <h1>Penyewaan Gedung Z</h1>
    <nav>
      <ul>
        <li><a href="index.php">Beranda</a></li>
        <li><a href="index.php #services">Layanan</a></li>
        <li><a href="index.php #contact">Kontak</a></li>
        <li><a href="orders.php">Daftar Pesanan</a></li>
      </ul>
    </nav>
  </header>

  <section id="orders">
    <h2>Daftar Pesanan</h2>
    <?php
    session_start();
    if (isset($_SESSION['success_message'])) {
      echo '<p class="success">' . $_SESSION['success_message'] . '</p>';
      unset($_SESSION['success_message']);
    }

    if (isset($_SESSION['error_message'])) {
      echo '<p class="error">' . $_SESSION['error_message'] . '</p>';
      unset($_SESSION['error_message']);
    }

    include 'config.php';
    // Pastikan Anda telah membuat koneksi ke database
    $conn = new mysqli($host, $username, $password, $dbname);

    if ($conn->connect_error) {
      die("Koneksi ke database gagal: " . $conn->connect_error);
    }

    $sql = "SELECT * FROM orders";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
      echo '<ul>';
      while ($row = $result->fetch_assoc()) {
        echo '<li>';
        echo '<strong>' . $row['name'] . '</strong> - Tanggal Acara: ' . $row['event_date'] . ', Jenis Ruangan: ' . $row['room_type'];
        echo '<a href="edit_order.php?id=' . $row['id'] . '">Edit</a>';
        echo '<a href="delete_order.php?id=' . $row['id'] . '">Hapus</a>';
        echo '</li>';
      }
      echo '</ul>';
    } else {
      echo '<p>Belum ada pesanan.</p>';
    }

    $conn->close();
    ?>
  </section>

  <footer>
  <p>kelompok 6</p>
  </footer>
</body>
</html>
