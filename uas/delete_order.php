<?php
session_start();
include 'config.php';
$conn = new mysqli($host, $username, $password, $dbname);

if ($conn->connect_error) {
  die("Koneksi ke database gagal: " . $conn->connect_error);
}

if ($_SERVER['REQUEST_METHOD'] === 'GET' && isset($_GET['id'])) {
  $orderId = $_GET['id'];
  $sql = "DELETE FROM orders WHERE id=$orderId";

  if ($conn->query($sql) === true) {
    $_SESSION['success_message'] = 'Pesanan berhasil dihapus.';
  } else {
    $_SESSION['error_message'] = 'Gagal menghapus pesanan: ' . $conn->error;
  }
}

$conn->close();
header('Location: orders.php');
exit;
