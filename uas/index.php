<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Penyewaan Gedung Z</title>
  <link rel="stylesheet" href="style.css">
</head> 
<body>
  <header>
    <h1>Penyewaan Gedung Z</h1>
    <nav>
      <ul>
        <li><a href="#home">Beranda</a></li>
        <li><a href="#services">Layanan</a></li>
        <li><a href="#contact">Kontak</a></li>
        <li><a href="orders.php">Daftar Pesanan</a></li>
      </ul>
    </nav>
  </header>

  <section id="home">
    <section id="home">
        <h2>Selamat Datang di Penyewaan Gedung Z</h2>
        <div class="image-container">
            <img src="gedung1.jpg" alt="Gambar 1" class="home-image">
            <img src="gedung2.jpg" alt="Gambar 2" class="home-image">
            <img src="gedung3.jpg" alt="Gambar 3" class="home-image">
          </div>
        <p>
          Selamat datang di Penyewaan Gedung Z! Kami menyediakan berbagai jenis ruangan untuk berbagai acara seperti pertemuan bisnis,
          pernikahan, seminar, dan acara lainnya. Dengan fasilitas yang nyaman dan pelayanan terbaik dari tim kami, kami siap untuk
          memastikan acara Anda berjalan lancar dan tak terlupakan.
        </p>
        <p>
          Silakan jelajahi layanan kami dan isi formulir pemesanan di bawah ini untuk menyewa ruangan yang sesuai dengan kebutuhan acara Anda.
        </p>
       
      </section>
      
      
  </section>
  <br></br>
  <br></br>
  
    <section id="services">
        <h2>Layanan Kami</h2>
        <div class="image-container">
            <img src="1.jpg" alt="1" class="services-image">
            <img src="2.jpg" alt="2" class="services-image">
            <img src="3.jpg" alt="3" class="services-image">
          </div>
        <p>
          Kami menawarkan berbagai jenis ruangan yang dapat disesuaikan dengan kebutuhan acara Anda. Berikut adalah beberapa jenis ruangan
          yang tersedia:
        </p>
        <ul>
          <li>Ruang Pertemuan: Ruangan yang cocok untuk pertemuan bisnis atau presentasi.</li>
          <li>Gedung Pernikahan: Ruangan indah untuk merayakan momen bahagia pernikahan Anda.</li>
          <li>Aula Acara: Aula luas yang cocok untuk berbagai acara besar seperti pameran, konser, atau perayaan.</li>
          <li>Ruang Seminar: Ruangan yang nyaman untuk kegiatan pelatihan, workshop, atau seminar.</li>
        </ul>
      </section>
      

  <br></br>
  <br></br>
  <section id="contact">
    <h2>Hubungi Kami</h2>
    <p>Silakan hubungi kami untuk informasi lebih lanjut tentang penyewaan gedung:</p>
    <p>Email: info@penyewaangedungxyz.com</p>
    <p>Telepon: (123) 456-7890</p>
  </section>
  <br></br>
  <br></br>
  <section id="orders">
    <h2>Formulir Pemesanan</h2>
    <form action="proses_order.php" method="post">
      <label for="name">Nama:</label>
      <input type="text" name="name" id="name" required>

      <label for="eventDate">Tanggal Acara:</label>
      <input type="date" name="eventDate" id="eventDate" required>

      <label for="roomType">Jenis Ruangan:</label>
      <select name="roomType" id="roomType" required>
        <option value="">Pilih Jenis Ruangan</option>
        <option value="Ruang Pertemuan">Ruang Pertemuan</option>
        <option value="Gedung Pernikahan">Gedung Pernikahan</option>
        <option value="Aula Acara">Aula Acara</option>
        <option value="Ruang Seminar">Ruang Seminar</option>
      </select>

      <button type="submit">Pesan</button>
    </form>
  </section>
  <br></br>
  <br></br>
  <footer>
    <p>kelompok 6</p>
  </footer>
</body>
</html>
