<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Edit Pesanan - Penyewaan Gedung Z</title>
  <link rel="stylesheet" href="style.css">
</head>
<body>
  <header>
    <h1>Penyewaan Gedung Z</h1>
    <nav>
      <ul>
        <li><a href="index.php">Beranda</a></li>
        <li><a href="#services">Layanan</a></li>
        <li><a href="#contact">Kontak</a></li>
        <li><a href="orders.php">Daftar Pesanan</a></li>
      </ul>
    </nav>
  </header>

  <section id="editOrder">
    <?php
    session_start();
    include 'config.php';
    $conn = new mysqli($host, $username, $password, $dbname);

    if ($conn->connect_error) {
      die("Koneksi ke database gagal: " . $conn->connect_error);
    }

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
      if (
        isset($_POST['orderId']) && isset($_POST['name']) &&
        isset($_POST['eventDate']) && isset($_POST['roomType']) &&
        !empty($_POST['name']) && !empty($_POST['eventDate']) &&
        !empty($_POST['roomType'])
      ) {
        $orderId = $_POST['orderId'];
        $name = $_POST['name'];
        $eventDate = $_POST['eventDate'];
        $roomType = $_POST['roomType'];

        $sql = "UPDATE orders SET name='$name', event_date='$eventDate', room_type='$roomType' WHERE id=$orderId";

        if ($conn->query($sql) === true) {
          $_SESSION['success_message'] = 'Pesanan berhasil diperbarui.';
        } else {
          $_SESSION['error_message'] = 'Gagal memperbarui pesanan: ' . $conn->error;
        }
      }
      header('Location: orders.php');
      exit;
    }

    if ($_SERVER['REQUEST_METHOD'] === 'GET' && isset($_GET['id'])) {
      $orderId = $_GET['id'];
      $sql = "SELECT * FROM orders WHERE id=$orderId";
      $result = $conn->query($sql);

      if ($result->num_rows > 0) {
        $order = $result->fetch_assoc();
        echo '<h2>Edit Pesanan</h2>';
        echo '<form action="edit_order.php" method="post">';
        echo '<input type="hidden" name="orderId" value="' . $order['id'] . '">';
        echo '<label for="name">Nama:</label>';
        echo '<input type="text" name="name" id="name" value="' . $order['name'] . '" required>';

        echo '<label for="eventDate">Tanggal Acara:</label>';
        echo '<input type="date" name="eventDate" id="eventDate" value="' . $order['event_date'] . '" required>';

        echo '<label for="roomType">Jenis Ruangan:</label>';
        echo '<select name="roomType" id="roomType" required>';
        echo '<option value="">Pilih Jenis Ruangan</option>';
        $roomTypes = ['Ruang Pertemuan', 'Gedung Pernikahan', 'Aula Acara', 'Ruang Seminar'];
        foreach ($roomTypes as $roomType) {
          echo '<option value="' . $roomType . '"';
          if ($order['room_type'] === $roomType) {
            echo ' selected';
          }
          echo '>' . $roomType . '</option>';
        }
        echo '</select>';

        echo '<button type="submit">Simpan</button>';
        echo '</form>';
      } else {
        echo '<p>Pesanan tidak ditemukan.</p>';
      }
    }

    $conn->close();
    ?>
  </section>

  <footer>
  <p>kelompok 6</p>
  </footer>
</body>
</html>
