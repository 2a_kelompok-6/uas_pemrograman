<?php
session_start();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  if (
    isset($_POST['orderId']) && isset($_POST['name']) &&
    isset($_POST['eventDate']) && isset($_POST['roomType']) &&
    !empty($_POST['name']) && !empty($_POST['eventDate']) &&
    !empty($_POST['roomType'])
  ) {
    $orderId = $_POST['orderId'];
    $orderIndex = array_search($orderId, array_column($_SESSION['orders'], 'id'));

    if ($orderIndex !== false) {
      $_SESSION['orders'][$orderIndex]['name'] = $_POST['name'];
      $_SESSION['orders'][$orderIndex]['eventDate'] = $_POST['eventDate'];
      $_SESSION['orders'][$orderIndex]['roomType'] = $_POST['roomType'];
    }
  }
}

header('Location: orders.php');
